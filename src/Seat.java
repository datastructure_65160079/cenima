import java.util.Scanner;

public class Seat {
    Scanner kb = new Scanner(System.in);
    private String[][] ListSeat={{"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10"},
                                {"B1","B2","B3","B4","B5","B6","B7","B8","B9","B10"},
                                {"C1","C2","C3","C4","C5","C6","C7","C8","C9","C10"},
                                {"P1","P2","P3","P4","P5","P6"}};
    String[] IDSeat;
    int numSeat,price = 0;
    public void reListSeat(){//กลับไปก่อนที่จะเลือก
        for (int i = 0; i < IDSeat.length; i++) {
            if(IDSeat[i].charAt(0)=='A'){
                ListSeat[0][Character.getNumericValue(IDSeat[i].charAt(1))-1]=IDSeat[i];          
        }if(IDSeat[i].charAt(0)=='B'){
                ListSeat[1][Character.getNumericValue(IDSeat[i].charAt(1))-1]=IDSeat[i];          
        }if(IDSeat[i].charAt(0)=='C'){
                ListSeat[2][Character.getNumericValue(IDSeat[i].charAt(1))-1]=IDSeat[i];          
        }else{
                ListSeat[3][Character.getNumericValue(IDSeat[i].charAt(1))-1]=IDSeat[i];  
        }
        reSetPrice();
    }
    }
    private void showListSeat(){//แสดงที่นั่ง
        for (int i = 0; i < 4; i++) {
            if(i==3){
                System.out.print("      ");
            }
            System.out.print("| ");
            for (int j = 0; j < ListSeat[i].length; j++) {
                System.out.print(ListSeat[i][j]+ " ");
            }
            System.out.print(" |");
            System.out.println();
        }
    }
    private void inputNumSeat(){//รับค่าที่จำนวนที่นั่ง
        System.out.print("input number of Seat wanted : ");
        numSeat = kb.nextInt();
        IDSeat = new String[numSeat];
        }
    private void  inputIDSeat(){//รับเลขที่นั่งและอัปเดตที่นั่ง
        System.out.print("input IDseat : ");
        for (int i = 0; i < IDSeat.length; i++) {
            IDSeat[i] = kb.next();
            int row = Character.getNumericValue(IDSeat[i].charAt(1))-1;
            int col=0;
            if(IDSeat[i].charAt(0)!='P'){
            if (IDSeat[i].charAt(0)=='A') {
                col=0;
            }if (IDSeat[i].charAt(0)=='B') {
                col=1;
            }if (IDSeat[i].charAt(0)=='C') {
                col=2;
            }
            }else{
                col=3;
            }
            if(ListSeat[col][row]=="XX"){
                System.out.println("Error");
                inputIDSeat();
            }
        if(IDSeat[i].charAt(0)!='P'){
            if (IDSeat[i].charAt(0)=='A') {
                ListSeat[0][row] = "XX";
            }if (IDSeat[i].charAt(0)=='B') {
                ListSeat[1][row] = "XX";
            }if (IDSeat[i].charAt(0)=='C') {
                ListSeat[2][row] = "XX";
            }
            price+=150;
        }else{
            ListSeat[3][row]="XX";
            price+=250;
        }
        }
    }
    public void price(){//ราคา
        for (int i = 0; i < IDSeat.length; i++) {
            if(IDSeat[i].charAt(0)!='P'){
                System.out.println(IDSeat[i]+", 150 baht");
            }else{
                System.out.println(IDSeat[i]+", 250 baht");
            }
        }
    }
    public void discount(){//ลดราคา
        price = price-(price*10/100);
    }
    public void showPrice(){//แสดงราคา
        System.out.println("total "+price + " Baht");
        System.out.println("----------------------------------------------------------------");
    }
    public int getPrice(){//return ราคา
        return price;
    }
    private void reSetPrice(){
        price=0;
    }
    public void seat(){//จัดการการเลือกที่นั่ง
        inputNumSeat();
        showListSeat();
        inputIDSeat();
        showListSeat();
        price();
     }
}